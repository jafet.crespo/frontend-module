import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CalculatorButtomComponent } from './calculator-buttom.component';

describe('CalculatorButtomComponent', () => {
  let component: CalculatorButtomComponent;
  let fixture: ComponentFixture<CalculatorButtomComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CalculatorButtomComponent]
    });
    fixture = TestBed.createComponent(CalculatorButtomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
