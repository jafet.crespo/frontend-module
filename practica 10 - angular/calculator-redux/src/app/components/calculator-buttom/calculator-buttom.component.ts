import { Component, Input } from '@angular/core';
import { Store } from '@ngrx/store';
import {
  addToDisplay,
  calculate,
  clearDisplay,
} from 'src/app/actions/calculator.actions';
import { CalculatorState } from '../../reducers/calculator.reducer';
@Component({
  selector: 'app-calculator-buttom',
  templateUrl: './calculator-buttom.component.html',
  styleUrls: ['./calculator-buttom.component.less'],
  template: '',
})
export class CalculatorButtomComponent {
  constructor(private store: Store<{ calculator: CalculatorState }>) {}

  @Input() button: string = '';
  onClick(button: string) {
    if (button === '=') this.store.dispatch(calculate());
    else if (button === 'C') this.store.dispatch(clearDisplay());
    else this.store.dispatch(addToDisplay({ value: button }));
  }
}
