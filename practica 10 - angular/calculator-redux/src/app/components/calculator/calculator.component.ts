import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import {
  addToDisplay,
  calculate,
  clearDisplay,
} from 'src/app/actions/calculator.actions';
import { CalculatorState } from '../../reducers/calculator.reducer';

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.less'],
})
export class CalculatorComponent {
  buttons: string[] = [
    '7',
    '8',
    '9',
    '/',
    '4',
    '5',
    '6',
    '*',
    '1',
    '2',
    '3',
    '-',
    '0',
    '.',
    '=',
    '+',
    'C',
  ];
}
