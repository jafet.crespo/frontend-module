import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { CalculatorState } from '../../reducers/calculator.reducer';

@Component({
  selector: 'app-display',
  templateUrl: './display.component.html',
  styleUrls: ['./display.component.less'],
})
export class DisplayComponent {
  constructor(private store: Store<{ calculator: CalculatorState }>) {}
  displayValue$ = this.store.select((state) => state.calculator.displayValue);
}
