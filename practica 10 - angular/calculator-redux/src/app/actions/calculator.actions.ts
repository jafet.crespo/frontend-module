import { createAction, props } from '@ngrx/store';

export const addToDisplay = createAction(
  '[Calculator] Add to Display',
  props<{ value: string }>()
);

export const clearDisplay = createAction('[Calculator] Clear Display');

export const calculate = createAction('[Calculator] Calculate');
