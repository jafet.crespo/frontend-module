import { createReducer, on } from '@ngrx/store';
import * as CalculatorActions from '../actions/calculator.actions';

export interface CalculatorState {
  displayValue: string;
}

export const initialState: CalculatorState = {
  displayValue: '',
};

export const calculatorReducer = createReducer(
  initialState,
  on(CalculatorActions.addToDisplay, (state, { value }) => ({
    ...state,
    displayValue: state.displayValue + value,
  })),
  on(CalculatorActions.clearDisplay, (state) => ({
    ...state,
    displayValue: '',
  })),
  on(CalculatorActions.calculate, (state) => ({
    ...state,
    displayValue: eval(state.displayValue),
  }))
);
