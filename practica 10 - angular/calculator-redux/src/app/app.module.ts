import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CalculatorButtomComponent } from './components/calculator-buttom/calculator-buttom.component';
import { CalculatorComponent } from './components/calculator/calculator.component';
import { StoreModule } from '@ngrx/store';
import { calculatorReducer } from './reducers/calculator.reducer';
import { DisplayComponent } from './components/display/display.component';

@NgModule({
  declarations: [AppComponent, CalculatorButtomComponent, CalculatorComponent, DisplayComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    StoreModule.forRoot({ calculator: calculatorReducer }),
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
