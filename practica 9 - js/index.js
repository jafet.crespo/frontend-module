/* function coinCombinations(price) {
  if (price <= 0) return "";

  const coins = [1, 2, 5, 10, 20, 50, 100, 200];
  const dp = new Array(price + 1).fill(0);
  dp[0] = 1;
  for (let i = 0; i < coins.length; i++) {
    //if(coins[i] >= price ) return dp[price];
    for (let j = coins[i]; j <= price; j++) {
      dp[j] = dp[j] + dp[j - coins[i]];
    }
    console.log(dp);
  }
  console.log(dp);
  return dp[price];
}

console.log(coinCombinations(15)); */

function nBonacci(n, m) {
  if ((n > 1 && n <= 20) && (m > 1 && m <= 50)) {
    if (m < n) return 0;
    if (m === n) return 1;

    let series = new Array(m).fill(0);
    series[n - 1] = 1;
    for (let i = n; i < m; i++) {
      let sum = 0;
      for (let j = i - n; j < i; j++) {
        sum += series[j];
      }
      series[i] = sum;
    }
    console.log(series);
    return series[m - 1];
  }
  return;
}

//console.log(nBonacci(3, 6)); 
console.log(nBonacci(20, 51));
