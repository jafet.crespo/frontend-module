const $display = document.querySelector(".calculator__display");

function addToDisplay($display, value) {
  return $display.value + value;
}

function calculate(value) {
  return eval(value);
}

function clearDisplay() {
  return "";
}

const click = document.addEventListener("click", (e) => {
  e.stopPropagation();
  const $button = e.target;
  if ($button.classList.contains("calculator__button")) {
    if ($button.textContent === "=") $display.value = calculate($display.value);
    else if ($button.textContent === "C") $display.value = clearDisplay();
    else $display.value = addToDisplay($display, $button.textContent);
  }
});

module.exports = {
  addToDisplay,
  calculate,
  clearDisplay,
};
