const { test, expect } = require("@jest/globals");
const puppeteer = require("puppeteer");

describe("Calculadora", () => {
  let browser;
  let page;

  beforeAll(async () => {
    browser = await puppeteer.launch({ headless: "new" });
    page = await browser.newPage();
    await page.goto(
      "http://127.0.0.1:5500/practica%2011%20-%20unit%20tests/task11/index.html"
    );
  });

  afterAll(async () => {
    await browser.close();
  });

  test("Button C clears the display", async () => {
    await page.click(".calculator__button:last-child");
    const displayValue = await page.$eval(
      ".calculator__display",
      (el) => el.value
    );
    expect(displayValue).toBe("");
  });

  test("The number buttons add the value to the display", async () => {
    await page.click(".calculator__button:nth-child(9)");
    await page.click(".calculator__button:nth-child(10)");
    await page.click(".calculator__button:nth-child(11)");
    await page.click(".calculator__button:nth-child(5)");
    await page.click(".calculator__button:nth-child(6)");
    await page.click(".calculator__button:nth-child(7)");
    await page.click(".calculator__button:nth-child(1)");
    await page.click(".calculator__button:nth-child(2)");
    await page.click(".calculator__button:nth-child(3)");
    await page.click(".calculator__button:nth-child(13)");
    const displayValue = await page.$eval(
      ".calculator__display",
      (el) => el.value
    );
    expect(displayValue).toBe("1234567890");
  });

  test("The sum of 2 + 2 shows the value 4 on the display", async () => {
    await page.click(".calculator__button:last-child");
    await page.click(".calculator__button:nth-child(10)");
    await page.click(".calculator__button:nth-child(16)");
    await page.click(".calculator__button:nth-child(10)");
    await page.click(".calculator__button:nth-child(15)");
    const displayValue = await page.$eval(
      ".calculator__display",
      (el) => el.value
    );
    expect(displayValue).toBe("4");
  });

  test("The division of 2 / 2 shows the value 1 on the display", async () => {
    await page.click(".calculator__button:last-child");
    await page.click(".calculator__button:nth-child(10)");
    await page.click(".calculator__button:nth-child(4)");
    await page.click(".calculator__button:nth-child(10)");
    await page.click(".calculator__button:nth-child(15)");
    const displayValue = await page.$eval(
      ".calculator__display",
      (el) => el.value
    );
    expect(displayValue).toBe("1");
  });
});
