const { TextEncoder, TextDecoder } = require("util");
global.TextEncoder = TextEncoder;
global.TextDecoder = TextDecoder;
const { JSDOM } = require("jsdom");
const fs = require("fs");
const html = fs.readFileSync("task11/index.html", "utf-8");
const {
  addToDisplay,
  calculate,
  clearDisplay,
} = require("../task11/index.js");
dom = new JSDOM(html);
describe("", () => {
  test("Should be call to .calculator__display class", () => {
    //dom = new JSDOM(html);
    let display = dom.window.document.querySelector(".calculator__display");
    expect(display).toBeTruthy();
  });
});
describe("add to display function", () => {
  let dom;
  let display;
  let result;
  beforeEach(() => {
    // Setup a new JSDOM instance for each test
    dom = new JSDOM(html);
    // Initialize the display
    display = dom.window.document.querySelector(".calculator__display");
  });
  it("changes display value when addToDisplay is called", () => {
    let array = ["5", "+", "5"];
    result = "";
    for (let value of array) {
      result = result + addToDisplay(display, value);
    }
    expect(result).toBe("5+5");
  });
});
describe("calculate function", () => {
  let value;
  let result;
  it("sum operation", () => {
    value = "5+5";
    result = calculate(value);
    expect(result).toBe(10);
  });
  it("reduce operation", () => {
    value = "55-5";
    result = calculate(value);
    expect(result).toBe(50);
  });
  it("multiplication operation", () => {
    value = "5*5";
    result = calculate(value);
    expect(result).toBe(25);
  });
  it("division operation", () => {
    value = "5/5";
    result = calculate(value);
    expect(result).toBe(1);
  });
});
describe("clear display function", () => {
  let result;
  it("clear display", () => {
    result = "55";
    result = clearDisplay();
    expect(result).toBe("");
  });
});