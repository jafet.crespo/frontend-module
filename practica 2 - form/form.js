console.log("hola desde formulario");
const d = document;
const $reservationForm = d.getElementById("reservation-form");
let id;
//localStorage.setItem("reservations", JSON.stringify([]));
console.log($reservationForm);

$reservationForm.addEventListener("submit", (e) => {
  e.preventDefault();
  console.log("hola desde la enviada de formulario");
  const $inputs = d.querySelectorAll(
    '#reservation-form fieldset input:not([type=radio]), #reservation-form fieldset input[type="radio"]:checked'
  );
  console.log($inputs);
  const reservation = {};
  $inputs.forEach((input) => {
    if (input.type === "radio") {
      reservation[input.name] = input.id;
    } else if (input.type === "checkbox") {
      reservation[input.name] = input.checked;
    } else {
      reservation[input.name] = input.value;
    }
  });
  let id;
  console.log(reservation);
  let storage = JSON.parse(localStorage.getItem("reservations"));
  if (!storage) {
    localStorage.setItem("reservations", JSON.stringify([]));
    storage = JSON.parse(localStorage.getItem("reservations"));
    id = 0;
  }
  reservation["id"] = id++;
  storage.push(reservation);
  localStorage.setItem("reservations", JSON.stringify(storage));
  alert("Reservation submitted!");
  $reservationForm.reset();
});
