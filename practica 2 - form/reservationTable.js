let reservations = JSON.parse(localStorage.getItem("reservations"));

const $table = document.getElementById("reservation-table");
const d = document;

window.addEventListener("storage", (e) => {
  if (e.key === "reservations") {
    location.reload();
  }
});

d.addEventListener("DOMContentLoaded", (e) => {
  getAll();
  deleteClick();
});

function getAll() {
  const $fragment = document.createDocumentFragment();
  reservations.forEach((reservation) => {
    const newRow = createReservationRow(reservation);
    $fragment.appendChild(newRow);
  });
  replaceReservationData($fragment);
}

function createReservationRow(reservation) {
  const newRow = d.createElement("tr");
  for (const prop in reservation) {
    if (prop === "smoking") {
      const newCol = d.createElement("td");
      if (reservation[prop]) {
        newCol.insertAdjacentHTML(
          "afterbegin",
          `<img src="smoking.jpg" width="30px" height="30px" alt="smoking">`
        );
      } else {
        newCol.insertAdjacentHTML(
          "afterbegin",
          `<img src="noSmoking.jpg" width="30px" height="30px" alt="smoking">`
        );
      }
      newRow.appendChild(newCol);
    } else if (prop !== "id") {
      const newCol = d.createElement("td");
      newCol.textContent = reservation[prop];
      newRow.appendChild(newCol);
    }
  }
  console.log(newRow);
  let $deleteBtn = d.createElement("td");
  $deleteBtn.insertAdjacentHTML(
    "afterbegin",
    `<button class="delete" data-id=${reservation["id"]}>Remove</button>`
  );
  console.log(newRow);
  newRow.appendChild($deleteBtn);
  return newRow;
}

function replaceReservationData(data) {
  const $newReservationData = d.createElement("tbody");
  $newReservationData.setAttribute("id", "reservation-data");
  $newReservationData.appendChild(data);

  const $oldReservationData = d.getElementById("reservation-data");
  const $table = $oldReservationData.parentNode;
  $table.replaceChild($newReservationData, $oldReservationData);
}

function deleteClick() {
  d.addEventListener("click", async (e) => {
    if (e.target.matches(".delete")) {
      deleteReservation(e.target.dataset.id);
    }
  });
}

function deleteReservation(id) {
  let isDelete = confirm(`Are you sure you want to delete this data?`);
  if (isDelete) {
    const reservations = JSON.parse(localStorage.getItem("reservations"));
    const deleteIndex = reservations.findIndex(
      (reservation) => reservation.id === id
    );
    reservations.splice(deleteIndex, 1);
    localStorage.setItem("reservations", JSON.stringify(reservations));
    location.reload();
  }
}
