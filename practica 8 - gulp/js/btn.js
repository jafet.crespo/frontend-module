const $btn = document.querySelector(".card__count");
const $counter = document.querySelector(".card__number");

let count = 0;

$btn.addEventListener("click", (e) => {
  count++;
  $counter.textContent = count;
});
