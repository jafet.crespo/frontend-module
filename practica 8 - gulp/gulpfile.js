'use strict';
const gulp = require('gulp');
const sass = require('gulp-sass')(require('sass'));
const autoprefixer = require('gulp-autoprefixer');
const sourcemaps = require('gulp-sourcemaps');
const concat = require('gulp-concat');
const terser = require('gulp-terser');

let imagemin;

(async () => {
  imagemin = (await import('gulp-imagemin')).default;
})();

const inject = require('gulp-inject');


var browserSync = require('browser-sync').create();

gulp.task('server', function (done) {
    browserSync.init({
        server: {
            baseDir: './'
        },
        port: 3500      
    });
    done();
});

gulp.task('sass', () =>
    gulp
        .src('./css/*.scss')
        .pipe(sourcemaps.init())
        .pipe(
            sass({
                outputStyle: 'expanded', // nested, compact, compressed, expanded
            }).on('error', sass.logError)
        )
        .pipe(autoprefixer({
            versions: ['last 2 browsers']
        }))
        .pipe(gulp.dest('./dist'))
    );

gulp.task('scripts', () =>
  gulp
    .src('./js/*.js')
    .pipe(concat('bundle.js'))
    .pipe(terser())
    .pipe(gulp.dest('./dist'))
);

gulp.task('images', () =>
  gulp
    .src('./img/*')
    .pipe(imagemin())
    .pipe(gulp.dest('./dist'))
);

gulp.task('html', () => {
    const target = gulp.src('./index.html');
    const sources = gulp.src(['./dist/bundle.js', './dist/styles.css'], { read: false });
  
    return target
      .pipe(inject(sources, { relative: true }))
      .pipe(gulp.dest('./dist'));
  });
  

// gulp.task('default', gulp.series('sass', 'server','scripts', 'images', 'html', (done) => {
//     gulp.watch('./css/*.scss', gulp.series('sass'));
//     gulp.watch('./js/**/*.js', gulp.series('scripts'));
//     gulp.watch('./images/**/*', gulp.series('images'));
//     gulp.watch('./index.html',gulp.series('html')).on('change', browserSync.reload);
//     //gulp.watch(['./dist/index.html', './dist/*.js', './dist/css/*.scss'], gulp.series('html')).on('change', browserSync.reload);
//     done();
// }));

gulp.task('default', gulp.series('sass', 'scripts', 'images', 'html', 'server', (done) => {
    // Monitorear cambios en los archivos SCSS, luego ejecutar la tarea 'sass' y recargar el navegador
    gulp.watch('./css/*.scss', gulp.series('sass', 'html')).on('change', browserSync.reload);
  
    // Monitorear cambios en los archivos JS, luego ejecutar la tarea 'scripts' y recargar el navegador
    gulp.watch('./js/**/*.js', gulp.series('scripts', 'html')).on('change', browserSync.reload);
  
    // Monitorear cambios en los archivos HTML, luego ejecutar la tarea 'html' y recargar el navegador
    gulp.watch('./index.html', gulp.series('html')).on('change', browserSync.reload);
  
    // Monitorear cambios en los archivos de imágenes, luego ejecutar la tarea 'images' y recargar el navegador
    gulp.watch('./images/**/*', gulp.series('images')).on('change', browserSync.reload);
  
    done();
  }));
